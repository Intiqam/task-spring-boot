package com.task.student.controller;

import com.task.student.entity.Student;
import com.task.student.services.StudentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/students")
public class StudentController {
    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("/get")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok()
                .body(studentService.getAllStudents());
    }

    @GetMapping("get/{id}")
    public ResponseEntity<?> get(@PathVariable Long id) {
        return ResponseEntity.ok()
                .body(studentService.getById(id));
    }

    @PostMapping("/create")
    public void insert(@RequestBody Student student) {
        studentService.create(student);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Long id) {
        studentService.deleteById(id);
    }

    @PutMapping("/update")
    public ResponseEntity<?> update(@RequestBody Student student) {
        return ResponseEntity.ok().body(studentService.update(student));
    }
}
