package com.task.student.services;

import com.task.student.entity.Student;
import com.task.student.repository.StudentRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface StudentService {
    void create(Student student);

    List<Student> getAllStudents();

    Student getById(Long id);

    void deleteById(Long id);

    Student update(Student student);

}
