package com.task.student.services.impl;

import com.task.student.entity.Student;
import com.task.student.repository.StudentRepository;
import com.task.student.services.StudentService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    @Override
    public void create(Student student) {
        studentRepository.save(student);
    }

    @Override
    public Student getById(Long id) {
       return studentRepository.findById(id)
               .orElseThrow(RuntimeException::new);
    }

    @Override
    public void deleteById(Long id) {
        studentRepository.findById(id)
                .ifPresent(studentRepository::delete);
    }

    @Override
    public Student update(Student student) {
        return Optional.of(studentRepository.save(student))
                .orElseThrow(RuntimeException::new);
    }
}
