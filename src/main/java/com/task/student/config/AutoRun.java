package com.task.student.config;

import com.task.student.entity.Student;
import com.task.student.services.StudentService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class AutoRun {
    private final StudentService studentService;

    public AutoRun(StudentService studentService) {
        this.studentService = studentService;
    }
    @Bean
    public CommandLineRunner auto() {
        return args -> {
            studentService.create(new Student("John","MIT", LocalDate.of(1990,12,28)));
        };
    }
}
